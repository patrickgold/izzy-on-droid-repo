{"pn":"org/emdev/","lib":"EmDev","tp":"Development Aid","ch":""}
{"pn":"org/spongycastle/","lib":"Spongy Castle","tp":"Utility","ch":"http://rtyley.github.io/spongycastle/"}
{"pn":"repack/org/bouncycastle/","lib":"Bouncy Castle (repacked)","tp":"Utility","ch":"http://www.bouncycastle.org/java.html"}
{"pn":"com/dropbox/client2/","lib":"Dropbox API v2","tp":"Cloud Storage","ch":"http://www.dropbox.com/"}
{"pn":"com/smaato/soma/","lib":"Smaato","tp":"Advertisement","ch":"https://www.smaato.com/"}
{"pn":"net/lingala/zip4j/","lib":"Zip4j","tp":"Utility","ch":"http://www.lingala.net/zip4j/"}
{"pn":"uk/co/senab/photoview/","lib":"PhotoView","tp":"Utility","ch":"https://github.com/chrisbanes/PhotoView"}
{"pn":"com/stericson/RootTools/","lib":"Stericson RootTools","tp":"Utility","ch":"https://github.com/Stericson/RootTools"}
{"pn":"com/androidplot/","lib":"Androidplot","tp":"Utility","ch":"http://androidplot.com/"}
{"pn":"com/path/android/jobqueue/","lib":"Android Priority Job Queue","tp":"Utility","ch":"https://github.com/path/android-priority-jobqueue"}
{"pn":"com/bea/xml/stream/","lib":"StAX","tp":"Utility","ch":""}
{"pn":"org/yaml/snakeyaml/","lib":"SnakeYAML","tp":"Utility","ch":""}
{"pn":"ch/qos/logback/","lib":"Logback Classic Module","tp":"Utility","ch":"http://logback.qos.ch/"}
{"pn":"org/tukaani/xz/","lib":"XZ For Java","tp":"Utility","ch":"http://tukaani.org/xz/java.html"}
{"pn":"ch/boye/httpclientandroidlib/","lib":"httpclientandroidlib","tp":"Utility","ch":"https://github.com/surespot/httpclientandroidlib"}
{"pn":"com/google/common/","lib":"Google Core Libraries for Java 6+","tp":"Utility","ch":"https://github.com/google/guava"}
{"pn":"com/google/thirdparty/","lib":"Google Core Libraries (3rd Party)","tp":"Utility","ch":"https://github.com/google/guava"}
{"pn":"com/daimajia/androidanimations/","lib":"AndroidAnimations Library","tp":"Utility","ch":"https://github.com/daimajia/AndroidViewAnimations"}
{"pn":"com/google/android/apps/dashclock/api/","lib":"DashClock API","tp":"Development Aid","ch":"http://dashclock.com/api"}
{"pn":"com/koushikdutta/async/","lib":"AndroidAsync","tp":"Utility","ch":"https://github.com/koush/AndroidAsync"}
{"pn":"android/content/","lib":"Content sharing","tp":"Development Aid","ch":"https://developer.android.com/reference/android/content/package-summary.html"}
{"pn":"org/afzkl/colorpicker/","lib":"ColorPicker","tp":"Utility","ch":""}
{"pn":"afzkl/development/colorpickerview/","lib":"ColorPickerView","tp":"UI Component","ch":""}
{"pn":"org/achartengine/","lib":"AChartEngine","tp":"Utility","ch":"http://www.achartengine.org/"}
{"pn":"com/jcraft/jsch/","lib":"Java Secure Channel","tp":"Utility","ch":"http://www.jcraft.com/jsch/"}
{"pn":"org/teleal/cling/","lib":"Cling UPnP library","tp":"Utility","ch":"http://4thline.org/projects/cling/"}
{"pn":"com/mashape/relocation/","lib":"Mashape Relocation API","tp":"Utility","ch":""}
{"pn":"org/java_websocket/","lib":"Java-WebSocket","tp":"Utility","ch":"https://github.com/TooTallNate/Java-WebSocket"}
{"pn":"net/simonvt/menudrawer/","lib":"MenuDrawer","tp":"Utility","ch":"https://github.com/SimonVT/android-menudrawer"}
{"pn":"org/androidannotations/","lib":"AndroidAnnotations API","tp":"Utility","ch":"https://github.com/excilys/androidannotations"}
{"pn":"com/shaded/fasterxml/jackson/","lib":"Jackson Project","tp":"Utility","ch":"https://github.com/FasterXML/jackson"}
{"pn":"org/xbill/DNS/","lib":"Dnsjava","tp":"Utility","ch":"https://github.com/dnsjava/dnsjava"}
{"pn":"org/jaudiotagger/","lib":"JAudiotagger","tp":"Utility","ch":"http://www.jthink.net/jaudiotagger/"}
{"pn":"it/gmariotti/cardslib/","lib":"CardsLib","tp":"GUI support","ch":"https://github.com/gabrielemariotti/cardslib"}
{"pn":"freemarker/","lib":"Apache FreeMarker","tp":"Utility","ch":"http://freemarker.org/"}
{"pn":"lombok/","lib":"ProjectLombok","tp":"Utility","ch":"https://github.com/rzwitserloot/lombok"}
{"pn":"org/objectweb/asm/","lib":"ASM","tp":"Utility","ch":"http://asm.ow2.org/"}
{"pn":"com/facebook/ads/","lib":"Facebook Ads","tp":"Advertisement","ch":"https://developers.facebook.com/"}
{"pn":"com/flurry/android/ads/","lib":"Flurry Ads","tp":"Advertisement","ch":"http://www.flurry.com/"}
{"pn":"com/mopub/mobileads/","lib":"MoPub","tp":"Advertisement","ch":"http://www.mopub.com/"}
{"pn":"com/vividsolutions/jtsexample","lib":"JTS Topology Suite Examples","tp":"Development Framework","ch":";http://sourceforge.net/projects/jts-topo-suite"}
{"pn":"com/itextpdf/","lib":"iText","tp":"Utility","ch":"https://api.itextpdf.com/"}

-- -------------------------------------
-- Table structure of ApkCheck database
-- -------------------------------------
-- data types are for SQLite and need to be adjusted when using a different DB

--
-- scan results from VirusTotal and our local library scanner
--
CREATE TABLE files (
    local_file_name      VARCHAR,       -- file name as stored in our repo
    github_file_url      VARCHAR,       -- URL of the corresponding file at Github/GitLab/Codeberg …
    pkgname              VARCHAR,       -- package name of the corresponding app
    vt_permalink         VARCHAR,       -- URL to VirusTotal details on the file, if any
    vt_result            TEXT,          -- JSON with scan results (or empty)
    vt_detected          INT,           -- how many alerts have been detected by VirusTotal (check vt_result for details)
    apk_libraries        TEXT,          -- JSON with details on detected libraries other than pay,ad,analytics (or empty)
    apk_paymodules       TEXT,          -- JSON with details on detected payment services modules
    apk_admodules        TEXT,          -- JSON with details on detected ad modules (or empty)
    apk_analyticsmodules TEXT,          -- JSON with details on detected analytics modules (or empty)
    libcount             INT,           -- number of libraries altogether
    CONSTRAINT pk_files PRIMARY KEY (
        local_file_name
    )
);

--
-- queue of files pending results from VirusTotal
--
CREATE TABLE job_queue (
    local_file_name  VARCHAR,           -- file name as stored in our repo
    vt_json          TEXT,              -- JSON response from submit
    vt_hash          VARCHAR,           -- MD5/SHA256 hash identifying the resource
    vt_response_code INT,               -- current VT response code: 0:not present, 1:ready for retrieval (results embedded in JSON), -2:still enqueued
    entry_time       INT                -- when this file was added to the queue (Unix timestamp)
);

/* -- COMMENT ON is not supported by SQLite
COMMENT ON COLUMN files.local_file_name IS 'file name as stored in our repo';
COMMENT ON COLUMN files.github_file_url IS 'URL of the corresponding file at Github';
COMMENT ON COLUMN files.vt_permalink IS 'URL to VirusTotal details on the file, if any';
COMMENT ON COLUMN files.vt_result IS 'JSON with scan results (or empty)';
COMMENT ON COLUMN files.vt_detected IS 'how many alerts have been detected by VirusTotal (check vt_result for details)';
COMMENT ON COLUMN files.apk_libraries IS 'JSON with details on detected libraries (or empty)';
COMMENT ON COLUMN files.apk_admodules IS 'JSON with details on detected ad modules (or empty)';
COMMENT ON COLUMN files.apk_analyticsmodules IS 'JSON with details on detected analytics modules (or empty)';

COMMENT ON COLUMN job_queue.local_file_name IS 'file name as stored in our repo';
COMMENT ON COLUMN job_queue.vt_json IS 'JSON response from submit';
COMMENT ON COLUMN job_queue.vt_hash IS 'MD5 hash identifying the resource';
COMMENT ON COLUMN job_queue.vt_response_code IS '0:not present, 1:ready for retrieval (results embedded in JSON), -2:still enqueued';
*/

--
-- SQLite does not support ON DELETE CASCADE, so we need a trigger:
--
CREATE TRIGGER tr_del_abandoned_from_queue
  BEFORE DELETE ON files
  FOR EACH ROW BEGIN
    DELETE FROM job_queue WHERE local_file_name = old.local_file_name;
  END;

--
-- a view for a quick glance at our packages
--
CREATE VIEW app_states AS
  SELECT pkgname,                               -- package name of the app
         COUNT(local_file_name) AS filecount,   -- number of APK files
         SUM(vt_detected) AS vt_detected        -- number of threats detected by VirusTotal (sum for all files)
    FROM files
   GROUP BY pkgname
   ORDER BY pkgname;

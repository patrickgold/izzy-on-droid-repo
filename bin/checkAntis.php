#!/usr/bin/php
<?php
// ============================================================================
// AntiFeatures CrossChecker
// Cross-check database versus last package of each app and report discrepancies
// © Andreas Itzchak Rehberg
// ----------------------------------------------------------------------------
// This program is free software; you can redistribute and/or modify it under
// the terms of the GNU General Public License (see ../LICENSE)
// ============================================================================

$base = dirname(__DIR__);
require_once($base.'/lib/fdroid.class.php');
require_once($base.'/lib/apkcheck.class.php');
$conf = parse_ini_file($base.'/lib/apkscan.ini', TRUE); // parse with sections
$apkcheck = new apkcheck($conf['general']['apkdir'],$conf['general']['database']);
$apkcheck->initRadar($conf['libradar']['radarDir'],$conf['libradar']['libsFile'],$conf['libradar']['libsWildFile'],$conf['libradar']['libsSmaliFile']);
$libinfo = [];
foreach( file($conf['libradar']['libsInfoFile']) as $line ) { $a = json_decode($line); $libinfo[$a->id] = $a; }

$fdroid = new fdroid($base.'/repo',0);
$apps = $fdroid->getAppList();

$antis = ['Ads','ApplicationDebuggable','NonFreeAdd','NonFreeDep','NonFreeNet','Tracking'];
$globals = ['NonFreeAdd','NonFreeDep','NonFreeNet']; // not say "gone" when no such lib was found

foreach($apps as $app) {
  $str = ''; $res = [];
  foreach($antis as $anti) { $rec[$anti] = 0; $pkg[$anti] = 0; } // init
  // check our database
  if ( property_exists($app,'antifeatures') ) {
    $antifeat = explode(',',$app->antifeatures);
    foreach($antis as $anti) if (in_array($anti,$antifeat)) $rec[$anti] = 1;
  }
  // check latest package
  if ( is_array($app->package) ) {
    $apk = $app->package[0]->apkname;
    if (property_exists($app->package[0],'permissions')) $perms = explode(',',$app->package[0]->permissions); else $perms = [];
  } else {
    $apk = $app->package->apkname;
    if (property_exists($app->package,'permissions')) $perms = explode(',',$app->package->permissions); else $perms = [];
  }
  if ( !file_exists("{$base}/repo/${apk}") ) {
    echo "Skipping missing file '$apk'\n";
    continue;
  }
  $fileinfo = $apkcheck->getFile($apk);
  if ( !(empty($fileinfo['apk_admodules']) || $fileinfo['apk_admodules'] == '[]') ) $pkg['Ads'] = 1;
  if ( !(empty($fileinfo['apk_analyticsmodules']) || $fileinfo['apk_analyticsmodules'] == '[]') ) $pkg['Tracking'] = 1;
  $pkg['ApplicationDebuggable'] = trim( exec( "aapt d badging ../repo/${apk} | grep application-debuggable | wc -l" ) );
  // libradar doesn't know about (additional) AntiFeatures pointed out in libinfo.txt
  $all_libs = [];
  $all_libs = @array_merge(json_decode($fileinfo['apk_libraries']), json_decode($fileinfo['apk_admodules']), json_decode($fileinfo['apk_analyticsmodules']), json_decode($fileinfo['apk_paymodules']) );
  if ( !empty($all_libs) ) foreach ( $all_libs as $lib ) {
    if ( !empty($libinfo['/'.$lib->pkgname]->anti) ) foreach($libinfo['/'.$lib->pkgname]->anti as $la) {
      if ( in_array($la,['NonFreeNet','Tracking']) && !in_array('INTERNET',$perms) ) continue;
      $pkg[$la] = 1;
    }
  }

  // compare
  foreach($antis as $anti) {
    if     ( $rec[$anti]>$pkg[$anti] ) { if (!in_array($anti,$globals)) $res[] = "no longer ${anti}"; }
    elseif ( $rec[$anti]<$pkg[$anti] ) $res[] = "needs ${anti}";
  }
  if ( !empty($res) ) {
    $str = implode(', ',$res);
    $str = $app->id . ": ".$str;
    echo "${str}\n";
  }
}

?>